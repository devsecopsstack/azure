# GitLab CI template for Azure

This project implements a GitLab CI/CD template to deploy your application to [Azure](https://azure.microsoft.com/).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/azure/gitlab-ci-azure@2.2.3
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      base-app-name: wonderapp
      review-enabled: true
      staging-enabled: true
      prod-enabled: true
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/azure'
    ref: '2.2.3'
    file: '/templates/gitlab-ci-azure.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  AZURE_BASE_APP_NAME: wonderapp
  AZURE_REVIEW_ENABLED: "true"
  AZURE_STAGING_ENABLED: "true"
  AZURE_PROD_ENABLED: "true"
```

## Understand

This chapter introduces key notions and principle to understand how this template works.

### Managed deployment environments

This template implements continuous delivery/continuous deployment for projects hosted on Azure.

It allows you to manage automatic deployment & cleanup of standard predefined environments.
Each environment can be enabled/disabled by configuration.
If you're not satisfied with predefined environments and/or their associated Git workflow, you may implement you own environments and
workflow, by reusing/extending the base (hidden) jobs. This is advanced usage and will not be covered by this documentation.

The following chapters present the managed predefined environments and their associated Git workflow.

#### Review environments

The template supports **review** environments: those are dynamic and ephemeral environments to deploy your
_ongoing developments_ (a.k.a. _feature_ or _topic_ branches).

When enabled, it deploys the result from upstream build stages to a dedicated and temporary environment.
It is only active for non-production, non-integration branches.

It is a strict equivalent of GitLab's [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) feature.

It also comes with a _cleanup_ job (accessible either from the _environments_ page, or from the pipeline view).

#### Integration environment

If you're using a Git Workflow with an integration branch (such as [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)),
the template supports an **integration** environment.

When enabled, it deploys the result from upstream build stages to a dedicated environment.
It is only active for your integration branch (`develop` by default).

#### Production environments

Lastly, the template supports 2 environments associated to your production branch (`master` or `main` by default):

* a **staging** environment (an iso-prod environment meant for testing and validation purpose),
* the **production** environment.

You're free to enable whichever or both, and you can also choose your deployment-to-production policy:

* **continuous deployment**: automatic deployment to production (when the upstream pipeline is successful),
* **continuous delivery**: deployment to production can be triggered manually (when the upstream pipeline is successful).

### Supported authentication methods

The Azure template supports two kinds of authentication:

1. service principal authentication with credentials (user & password),
2. or [federated authentication using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/).

#### Service principal authentication with credentials

To use this authentication method, [configure a service principal](https://learn.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli) with proper permissions
to carry out your automated deployment tasks, then provide required credentials (username, password or certificate and tenant ID) as GitLab CI/CD secret variables (see doc below).

Can be provided globally and/or per environment.

#### Federated authentication using OpenID Connect

The Azure template supports [OpenID Connect to retrieve temporary credentials](https://docs.gitlab.com/ee/ci/cloud_services/azure/).

If you wish to use this authentication mode, please activate and configure the [OIDC variant](#oidc-variant). 

### Deployment context variables

In order to manage the various deployment environments, this template provides a couple of **dynamic variables**
that you might use in your hook scripts, deployment manifests and other deployment resources:

* `${environment_type}`: the current deployment environment type (`review`, `integration`, `staging` or `production`)
* `${environment_name}`: a generated application name to use for the current deployment environment (ex: `myapp-review-fix-bug-12` or `myapp-staging`) - _details below_

#### Generated environment name

The `${environment_name}` variable is generated to designate each deployment environment with a unique and meaningful application name.
By construction, it is suitable for inclusion in DNS, URLs, Kubernetes labels...
It is built from:

* the application _base name_ (defaults to `$CI_PROJECT_NAME` but can be overridden globally and/or per deployment environment - _see configuration variables_)
* GitLab predefined `$CI_ENVIRONMENT_SLUG` variable ([sluggified](https://en.wikipedia.org/wiki/Clean_URL#Slug) name, truncated to 24 characters)

The `${environment_name}` variable is then evaluated as:

* `<app base name>` for the production environment
* `<app base name>-$CI_ENVIRONMENT_SLUG` for all other deployment environments
* :bulb: `${environment_name}` can also be overriden per environment with the appropriate configuration variable

Examples (with an application's base name `myapp`):

| `$environment_type` | Branch        | `$CI_ENVIRONMENT_SLUG`  | `$environment_name` |
|---------------------|---------------|-------------------------|---------------------|
| `review`            | `feat/blabla` | `review-feat-bla-xmuzs6`| `myapp-review-feat-bla-xmuzs6` |
| `integration`       | `develop`     | `integration`           | `myapp-integration` |
| `staging`           | `main`        | `staging`               | `myapp-staging` |
| `production`        | `main`        | `production`            | `myapp` |

### Deployment and cleanup scripts

The Azure template requires you to provide a shell script that fully implements your application
deployment and cleanup using the [`azure` CLI](https://learn.microsoft.com/cli/azure/) and all other tools available in the selected Docker image.

The deployment script is searched as follows:

1. look for a specific `azure-deploy-$environment_type.sh` in the `$AZURE_SCRIPTS_DIR` directory in your project (e.g. `azure-deploy-staging.sh` for staging environment),
2. if not found: look for a default `azure-deploy.sh` in the `$AZURE_SCRIPTS_DIR` directory in your project,
3. if not found: the deployment job will fail.

The cleanup script is searched as follows:

1. look for a specific `azure-cleanup-$environment_type.sh` in the `$AZURE_SCRIPTS_DIR` directory in your project (e.g. `azure-cleanup-staging.sh` for staging environment),
2. if not found: look for a default `azure-cleanup.sh` in the `$AZURE_SCRIPTS_DIR` directory in your project,
3. if not found: the cleanup job will fail.

> :information_source: Your deployment (and cleanup) scripts have to be able to cope with various environments, each with different application names, exposed routes, settings, ...
> Part of this complexity can be handled by the lookup policies described above (ex: one script per env) and also by using available environment variables:
>
> 1. [deployment context variables](#deployment-context-variables) provided by the template:
>     * `${environment_type}`: the current environment type (`review`, `integration`, `staging` or `production`)
>     * `${environment_name}`: the application name to use for the current environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`)
>     * `${hostname}`: the environment hostname, extracted from the current environment url (after late variable expansion - see below)
> 2. any [GitLab CI variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
> 3. any [custom variable](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project)
>     (ex: `${SECRET_TOKEN}` that you have set in your project CI/CD variables)

### Environments URL management

The Azure template supports two ways of providing your environments url:

* a **static way**: when the environments url can be determined in advance, probably because you're exposing your routes through a DNS you manage,
* a [**dynamic way**](https://docs.gitlab.com/ee/ci/environments/#set-dynamic-environment-urls-after-a-job-finishes): when the url cannot be known before the
  deployment job is executed.

The **static way** can be implemented simply by setting the appropriate configuration variable(s) depending on the environment (see environments configuration chapters):

* `$AZURE_ENVIRONMENT_URL` to define a default url pattern for all your envs,
* `$AZURE_REVIEW_ENVIRONMENT_URL`, `$AZURE_INTEG_ENVIRONMENT_URL`, `$AZURE_STAGING_ENVIRONMENT_URL` and `$AZURE_PROD_ENVIRONMENT_URL` to override the default.

> :information_source: Each of those variables support a **late variable expansion mechanism** with the `%{somevar}` syntax, 
> allowing you to use any dynamically evaluated variables such as `${environment_name}`.
>
> Example:
>
> ```yaml
> variables:
>   AZURE_BASE_APP_NAME: "wonderapp"
>   # global url for all environments
>   AZURE_ENVIRONMENT_URL: "https://%{environment_name}.nonprod.acme.domain"
>   # override for prod (late expansion of $AZURE_BASE_APP_NAME not needed here)
>   AZURE_PROD_ENVIRONMENT_URL: "https://$AZURE_BASE_APP_NAME.acme.domain"
>   # override for review (using separate resource paths)
>   AZURE_REVIEW_ENVIRONMENT_URL: "https://wonderapp-review.nonprod.acme.domain/%{environment_name}"
> ```

To implement the **dynamic way**, your deployment script shall simply generate a `environment_url.txt` file in the working directory, containing only
the dynamically generated url. When detected by the template, it will use it as the newly deployed environment url.

### Deployment output variables

Each deployment job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

* `$environment_type`: set to the type of environment (`review`, `integration`, `staging` or `production`),
* `$environment_name`: the application name (see below),
* `$environment_url`: set to the environment URL (whether determined statically or dynamically).

Those variables may be freely used in downstream jobs (for instance to run acceptance tests against the latest deployed environment).

You may also add and propagate your own custom variables, by pushing them to the `azure.env` file in your [deployment script](#deployment-and-cleanup-scripts).

## Configuration reference

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:): 

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently 
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets 
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable), 
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`: 
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

### Global configuration

The Azure template uses some global configuration used throughout all jobs and environments.

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `cli-image` / `AZURE_CLI_IMAGE` | the Docker image used to run Azure CLI commands| `mcr.microsoft.com/azure-cli:latest` |
| `base-app-name` / `AZURE_BASE_APP_NAME` | Base application name                  | `$CI_PROJECT_NAME` ([see GitLab doc](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)) |
| `environment-url` / `AZURE_ENVIRONMENT_URL`  | Default environments url _(only define for static environment URLs declaration)_<br/>_supports late variable expansion (ex: `https://%{environment_name}.azure.acme.com`)_ | _none_ |
| `scripts-dir` / `AZURE_SCRIPTS_DIR` | Directory where Azure scripts (deploy & cleanup) are located | `.` _(root project dir)_ |
| `sp-client-id` / `AZURE_SP_CLIENT_ID` | Default Service Principal client ID _(only define if using Service Principal authentication with credentials)_ | _none_ |
| :lock: `AZURE_SP_PASSWORD`| Default Service Principal password (client secret or certificate (File type)) _(only define if using Service Principal authentication with credentials)_ | _none_ |
| `sp-tenant-id` / `AZURE_SP_TENANT_ID` | Default Service Principal tenant ID _(only define if using Service Principal authentication with credentials)_ | _none_ |

### Review environments configuration

Review environments are dynamic and ephemeral environments to deploy your _ongoing developments_ (a.k.a. _feature_ or 
_topic_ branches).

They are **disabled by default** and can be enabled by setting the `AZURE_REVIEW_ENABLED` variable (see below).

Here are variables supported to configure review environments:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `review-enabled` / `AZURE_REVIEW_ENABLED` | Azure project ID for `review` env | _none_ (disabled) |
| `review-app-name` / `AZURE_REVIEW_APP_NAME` | Application name for `review` env      | `"${AZURE_BASE_APP_NAME}-${CI_ENVIRONMENT_SLUG}"` (ex: `myproject-review-fix-bug-12`) |
| `review-environment-url` / `AZURE_REVIEW_ENVIRONMENT_URL`| The review environments url _(only define for static environment URLs declaration and if different from default)_ | `$AZURE_ENVIRONMENT_URL` |
| `review-sp-client-id` / `AZURE_REVIEW_SP_CLIENT_ID` | Service Principal client ID for `review` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| :lock: `AZURE_REVIEW_SP_PASSWORD`| Service Principal password (client secret or certificate (File type)) for `review` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| `review-sp-tenant-id` / `AZURE_REVIEW_SP_TENANT_ID` | Service Principal tenant ID for `review` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| `review-autostop-duration` / `AZURE_REVIEW_AUTOSTOP_DURATION` | The amount of time before GitLab will automatically stop `review` environments | `4 hours` |

### Integration environment configuration

The integration environment is the environment associated to your integration branch (`develop` by default).

It is **disabled by default** and can be enabled by setting the `AZURE_INTEG_ENABLED` variable (see below).

Here are variables supported to configure the integration environment:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `integ-enabled` / `AZURE_INTEG_ENABLED` | Azure project ID for `integration` env | _none_ (disabled) |
| `integ-app-name` / `AZURE_INTEG_APP_NAME` | Application name for `integration` env | `${AZURE_BASE_APP_NAME}-integration` |
| `integ-environment-url` / `AZURE_INTEG_ENVIRONMENT_URL`| The integration environment url _(only define for static environment URLs declaration and if different from default)_ | `$AZURE_ENVIRONMENT_URL` |
| `integ-sp-client-id` / `AZURE_INTEG_SP_CLIENT_ID` | Service Principal client ID for `integration` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| :lock: `AZURE_INTEG_SP_PASSWORD`| Service Principal password (client secret or certificate (File type)) for `integration` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| `integ-sp-tenant-id` / `AZURE_INTEG_SP_TENANT_ID` | Service Principal tenant ID for `integration` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |

### Staging environment configuration

The staging environment is an iso-prod environment meant for testing and validation purpose associated to your production 
branch (`main` or `master` by default).

It is **disabled by default** and can be enabled by setting the `AZURE_STAGING_ENABLED` variable (see below).

Here are variables supported to configure the staging environment:

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `staging-enabled` / `AZURE_STAGING_ENABLED` | Azure project ID for `staging` env | _none_ (disabled) |
| `staging-app-name` / `AZURE_STAGING_APP_NAME` | Application name for `staging` env     | `${AZURE_BASE_APP_NAME}-staging` |
| `staging-environment-url` / `AZURE_STAGING_ENVIRONMENT_URL`| The staging environment url _(only define for static environment URLs declaration and if different from default)_ | `$AZURE_ENVIRONMENT_URL` |
| `staging-sp-client-id` / `AZURE_STAGING_SP_CLIENT_ID` | Service Principal client ID for `staging` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| :lock: `AZURE_STAGING_SP_PASSWORD`| Service Principal password (client secret or certificate (File type)) for `staging` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| `staging-sp-tenant-id` / `AZURE_STAGING_SP_TENANT_ID` | Service Principal tenant ID for `staging` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |

### Production environment configuration

The production environment is the final deployment environment associated with your production branch (`main` or `master` by default).

It is **disabled by default** and can be enabled by setting the `AZURE_PROD_ENABLED` variable (see below).

Here are variables supported to configure the production environment:

| Input / Variable | Description                            | Default value     |
| ------------------------- | -------------------------------------- | ----------------- |
| `prod-enabled` / `AZURE_PROD_ENABLED` | Azure project ID for `production` env | _none_ (disabled) |
| `prod-app-name` / `AZURE_PROD_APP_NAME` | Application name for `production` env  | `$AZURE_BASE_APP_NAME` |
| `prod-environment-url` / `AZURE_PROD_ENVIRONMENT_URL`| The production environment url _(only define for static environment URLs declaration and if different from default)_ | `$AZURE_ENVIRONMENT_URL` |
| `prod-deploy-strategy` / `AZURE_PROD_DEPLOY_STRATEGY` | Defines the deployment to production strategy. One of `manual` (i.e. _one-click_) or `auto`. | `manual` |
| `prod-sp-client-id` / `AZURE_PROD_SP_CLIENT_ID` | Service Principal client ID for `production` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| :lock: `AZURE_PROD_SP_PASSWORD`| Service Principal password (client secret or certificate (File type)) for `production` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |
| `prod-sp-tenant-id` / `AZURE_PROD_SP_TENANT_ID` | Service Principal tenant ID for `production` env - _(only define if using Service Principal authentication with credentials and if different from default)_ | _none_ |

## Variants

The Azure template can be used in conjunction with template variants to cover specific cases.

### OIDC variant

This variant enables [OpenID Connect to retrieve temporary credentials](https://docs.gitlab.com/ee/ci/cloud_services/azure/).

If you wish to use this authentication mode, please follow carefully [the GitLab guide](https://docs.gitlab.com/ee/ci/cloud_services/azure/), 
then configure appropriately the related variables:

* `AZURE_OIDC_CLIENT_ID`+`AZURE_OIDC_TENANT_ID` for any global/common access,
* `AZURE_REVIEW_OIDC_CLIENT_ID`+`AZURE_REVIEW_OIDC_TENANT_ID` and/or `AZURE_INTEG_OIDC_CLIENT_ID`+`AZURE_INTEG_OIDC_TENANT_ID` and/or `AZURE_STAGING_OIDC_CLIENT_ID`+`AZURE_STAGING_OIDC_TENANT_ID` and/or `AZURE_PROD_OIDC_CLIENT_ID`+`AZURE_PROD_OIDC_TENANT_ID` if you wish to use a different role with any of your environments.

:warning: unlike other cloud providers, for now Azure doesn't support wildcards in federated identities subject identifier.
That means you'll be able to use it from **only one GitLab project and branch**. It restricts a lot its usability.

#### Configuration

The variant supports the following configuration:

| Input / Variable | Description                            | Default value     |
| ---------------------------- | -------------------------------------- | ----------------- |
| `oidc-aud` / `AZURE_OIDC_AUD` | The `aud` claim for the JWT | `api://AzureADTokenExchange` ([recommended default value](https://learn.microsoft.com/en-us/azure/active-directory/workload-identities/workload-identity-federation-create-trust?pivots=identity-wif-apps-methods-azp#important-considerations-and-restrictions)) |
| `oidc-client-id` / `AZURE_OIDC_CLIENT_ID` | Default Service Principal client ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) | _none_ (disabled)    |
| `oidc-tenant-id` / `AZURE_OIDC_TENANT_ID` | Default Service Principal tenant ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) | _none_ (disabled)    |
| `review-oidc-client-id` / `AZURE_REVIEW_OIDC_CLIENT_ID`| Service Principal client ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `review` env _(only define to override default)_ | _none_ (disabled) |
| `review-oidc-tenant-id` / `AZURE_REVIEW_OIDC_TENANT_ID`| Service Principal tenant ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `review` env _(only define to override default)_ | _none_ (disabled) |
| `integ-oidc-client-id` / `AZURE_INTEG_OIDC_CLIENT_ID` | Service Principal client ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `integration` env _(only define to override default)_ | _none_ (disabled) |
| `integ-oidc-tenant-id` / `AZURE_INTEG_OIDC_TENANT_ID` | Service Principal tenant ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `integration` env _(only define to override default)_ | _none_ (disabled) |
| `staging-oidc-client-id` / `AZURE_STAGING_OIDC_CLIENT_ID`| Service Principal client ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `staging` env _(only define to override default)_ | _none_ (disabled) |
| `staging-oidc-tenant-id` / `AZURE_STAGING_OIDC_TENANT_ID`| Service Principal tenant ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `staging` env _(only define to override default)_ | _none_ (disabled) |
| `prod-oidc-client-id` / `AZURE_PROD_OIDC_CLIENT_ID`  | Service Principal client ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `production` env _(only define to override default)_ | _none_ (disabled) |
| `prod-oidc-tenant-id` / `AZURE_PROD_OIDC_TENANT_ID`  | Service Principal tenant ID associated with GitLab to [authenticate using OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/azure/) on `production` env _(only define to override default)_ | _none_ (disabled) |

#### Example

```yaml
include:
  # main template
  - component: gitlab.com/to-be-continuous/azure/gitlab-ci-azure@2.2.3
  # OIDC variant
  - component: gitlab.com/to-be-continuous/azure/gitlab-ci-azure-oidc@2.2.3
    inputs:
      # common OIDC client ID & tenant ID for non-prod envs
      oidc-client-id: "<common client-id>"
      oidc-tenant-id: "<non-prod tenant-id>"
      # specific OIDC tenant ID for prod
      prod-oidc-tenant-id: "<prod tenant-id>"
```

### Vault variant

This variant allows delegating your secrets management to a [Vault](https://www.vaultproject.io/) server.

#### Configuration

In order to be able to communicate with the Vault server, the variant requires the additional configuration parameters:

| Input / Variable | Description                            | Default value     |
| ----------------- | -------------------------------------- | ----------------- |
| `TBC_VAULT_IMAGE` | The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use (can be overridden) | `registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest` |
| `vault-base-url` / `VAULT_BASE_URL` | The Vault server base API url          | _none_ |
| `vault-oidc-aud` / `VAULT_OIDC_AUD` | The `aud` claim for the JWT | `$CI_SERVER_URL` |
| :lock: `VAULT_ROLE_ID`   | The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID | **must be defined** |
| :lock: `VAULT_SECRET_ID` | The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID | **must be defined** |

#### Usage

Then you may retrieve any of your secret(s) from Vault using the following syntax:

```text
@url@http://vault-secrets-provider/api/secrets/{secret_path}?field={field}
```

With:

| Parameter                        | Description                            |
| -------------------------------- | -------------------------------------- |
| `secret_path` (_path parameter_) | this is your secret location in the Vault server |
| `field` (_query parameter_)      | parameter to access a single basic field from the secret JSON payload |

#### Example

```yaml
include:
  # main template
  - component: gitlab.com/to-be-continuous/azure/gitlab-ci-azure@2.2.3
  # Vault variant
  - component: gitlab.com/to-be-continuous/azure/gitlab-ci-azure-vault@2.2.3
    inputs:
      # audience claim for JWT
      vault-oidc-aud: "https://vault.acme.host"
      vault-base-url: "https://vault.acme.host/v1"
      # $VAULT_ROLE_ID and $VAULT_SECRET_ID defined as a secret CI/CD variable

variables:
  # Secrets managed by Vault
  AZURE_USERNAME: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/azure/prod/account?field=access_key_id"
  AZURE_PASSWORD: "@url@http://vault-secrets-provider/api/secrets/b7ecb6ebabc231/azure/prod/account?field=secret_access_key"
```
