## [2.2.3](https://gitlab.com/to-be-continuous/azure/compare/2.2.2...2.2.3) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([af9e646](https://gitlab.com/to-be-continuous/azure/commit/af9e646e992b37ff1003945686e81d48bebf0011))

## [2.2.2](https://gitlab.com/to-be-continuous/azure/compare/2.2.1...2.2.2) (2024-04-03)


### Bug Fixes

* **vault:** use vault-secrets-provider's "latest" image tag ([5fc7f73](https://gitlab.com/to-be-continuous/azure/commit/5fc7f7386438858b5da3a1e8794b979af865bac2))

## [2.2.1](https://gitlab.com/to-be-continuous/azure/compare/2.2.0...2.2.1) (2024-1-28)


### Bug Fixes

* AZURE_REVIEW_OIDC_TENANT_ID input mapping ([b4b4f0e](https://gitlab.com/to-be-continuous/azure/commit/b4b4f0ed028c0c80fbf70dd0646159e7c15153a5))

# [2.2.0](https://gitlab.com/to-be-continuous/azure/compare/2.1.0...2.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([1f887bf](https://gitlab.com/to-be-continuous/azure/commit/1f887bfc4f681b1dc2828bd79c79c1589c8d6388))

# [2.1.0](https://gitlab.com/to-be-continuous/azure/compare/2.0.2...2.1.0) (2023-12-8)


### Features

* use centralized service images (gitlab.com) ([125dcb4](https://gitlab.com/to-be-continuous/azure/commit/125dcb488f0de5144ebe50529d24fc860aa4bda7))

## [2.0.2](https://gitlab.com/to-be-continuous/azure/compare/2.0.1...2.0.2) (2023-12-2)


### Bug Fixes

* envsubst when variable contains a '&' ([ba1665e](https://gitlab.com/to-be-continuous/azure/commit/ba1665e84acd88450bd53cafd0264d4de04682f1))

## [2.0.1](https://gitlab.com/to-be-continuous/azure/compare/2.0.0...2.0.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([2e8d37c](https://gitlab.com/to-be-continuous/azure/commit/2e8d37c616e3d3c1afce7b8cb7924789edda5b8d))

# [2.0.0](https://gitlab.com/to-be-continuous/azure/compare/1.0.0...2.0.0) (2023-09-26)


* feat!: support environment auto-stop ([ba40513](https://gitlab.com/to-be-continuous/azure/commit/ba405136f8f3997f4fd536be5d903aff0cdd63c4))


### BREAKING CHANGES

* now review environments will auto stop after 4 hours
by default. Configurable (see doc).

# 1.0.0 (2023-09-12)


### Features

* initial commit ([6c37e7f](https://gitlab.com/to-be-continuous/azure/commit/6c37e7ff1f1feb455d1dab623f6ae529dd33110b))
